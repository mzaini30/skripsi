jurnal:

- latar belakang
- kerangka berpikir

santri -> siswa

**penelitian terdahulu**: jurnal

# Cara menyusun kerangka berpikir

<!-- - cari **skripsi** yang berkaitan dengan variabel y, ambil kerangka berpikir 1 paragraf pertamanya -->
<!-- - masukkan definisi variabel y kemudian aspek variabel y -->

<!-- - gunakan kalimat seperti ini: salah satu faktor yang mempengaruhi (variabel y) yaitu variable x1 -->
<!-- - definisi x1 -->
<!-- - aspek x1 **(beda yang bagian sini ya)** -->
<!-- - penelitian terdahulu yang menyatakan x1 terhadap y saling berhubungan -->

(ganti x2)

<!-- - gunakan kalimat seperti ini: salah satu faktor _lainnya_ yang mempengaruhi (variabel y) yaitu variable x1 -->
<!-- - definisi x1 -->
<!-- - aspek x1 **(beda yang bagian sini ya)** -->
<!-- - penelitian terdahulu yang menyatakan x1 terhadap y saling berhubungan -->

<!-- benang merah penelitian (gabungan dari x1, x2, dan y) -->

# Latar Belakang

<!-- - - cari **skripsi** yang berkaitan dengan variabel y, ambil latar belakang sampai ketemu definisi variabel y (fenomena disesuaikan dengan SMA Al Izzah) -->
<!-- - masukkan definisi variabel y kemudian aspek variabel y -->

<!-- - gunakan kalimat seperti ini: salah satu faktor yang mempengaruhi (variabel y) yaitu variable x1 -->
<!-- - definisi x1 -->
<!-- - faktor x1 -->
<!-- - penelitian terdahulu yang menyatakan x1 terhadap y saling berhubungan -->

(ganti x2)

<!-- - gunakan kalimat seperti ini: salah satu faktor _lainnya_ yang mempengaruhi (variabel y) yaitu variable x1 -->
<!-- - definisi x1 -->
<!-- - faktor x1 -->
<!-- - penelitian terdahulu yang menyatakan x1 terhadap y saling berhubungan -->

<!-- benang merah penelitian (gabungan dari x1, x2, dan y) -->

<!-- kesimpulan: berdasarkan ... atas peneliti ingin melakukan penelitian bla bla bla -->